const button = document.getElementById("findButton");
button.onclick = function () {
    let typedText = document.getElementById("input").value;
    let anagrams = getAnagramsOf(typedText).join(", ")
    
    let output = document.getElementById("output");
    output.textContent = anagrams
    output.setAttribute("style", "display: flex") //Default is display hidden, this shows the div on DOM
}
function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}
function getAnagramsOf(text) {
    let sortedText = alphabetize(text)
    let anagrams = []
    words.forEach(function(word) { //Filter through the words.js list
        if(word.length === sortedText.length) { //Don't bother testing words that are too long
            if(alphabetize(word) === sortedText){ // Compare alphabetized string
                anagrams.push(word) // Add word to the list
            }
        }
    })  
    return anagrams
}